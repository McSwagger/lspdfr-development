﻿using LSPD_First_Response.Mod.API;
using LSPD_First_Response.Mod.Callouts;
using Rage;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace SimCopter
{
    public class CarChase : Callout
    {
        //Suspects, their vehicle and the blips
        private List<Ped> _suspects;
        private Vehicle _suspectVehicle;
        private List<Blip> _blips;

        //Spawn location and pursuit
        private Vector3 _spawnLocation;
        private LHandle _pursuit;

        public override bool OnBeforeCalloutDisplayed()
        {

            return base.OnBeforeCalloutDisplayed();
        }

        public override bool OnCalloutAccepted()
        {
            return base.OnCalloutAccepted();
        }

        public override void OnCalloutDisplayed()
        {
            base.OnCalloutDisplayed();
        }

        public override void OnCalloutNotAccepted()
        {
            base.OnCalloutNotAccepted();
        }

        public override void Process()
        {
            base.Process();
        }

        public override void End()
        {
            base.End();
        }
    }
}
