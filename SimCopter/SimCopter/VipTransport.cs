﻿using LSPD_First_Response.Mod.Callouts;
using Rage;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using LSPD_First_Response.Mod.API;

namespace SimCopter
{
    [CalloutInfo("VipTransport", CalloutProbability.High)]
    public class VipTransport : TransportBase
    {
        public override bool OnBeforeCalloutDisplayed()
        {
            CalloutMessage = "VIP's requesting transport";
            PossibleLocations = Utility.VipLocations;

            return base.OnBeforeCalloutDisplayed();
        }
    }
}
