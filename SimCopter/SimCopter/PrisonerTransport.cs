﻿using LSPD_First_Response.Mod.Callouts;
using Rage;
using System;
using System.Collections.Generic;
using LSPD_First_Response.Engine.Scripting.Entities;
using LSPD_First_Response.Mod.API;

namespace SimCopter
{
    public enum TransportStatus
    {
        WaitingPickup,
        PassengersEntering,
        Enroute,
        PassengersLeaving,
        DroppedOff
    }

    [CalloutInfo("Prisoner Transport", CalloutProbability.High)]
    public class PrisonerTransport : TransportBase
    {
        public override bool OnBeforeCalloutDisplayed()
        {
            PossibleLocations = Utility.PoliceStations;

            PickupLocation = GetRandomLocation();
            DropoffLocation = GetRandomLocation(PossibleLocations.IndexOf(PickupLocation));

            Passengers = new List<Ped>
            {
                new Ped("S_M_Y_Cop_01", PickupLocation.WaitingArea.Around(2f), 360f),
                new Ped("S_M_Y_Cop_01", PickupLocation.WaitingArea.Around(2f), 360f),
                new Ped("S_M_Y_Prisoner_01", PickupLocation.WaitingArea.Around(2f), 360f)
            };
            return base.OnBeforeCalloutDisplayed();
        }
    }

    //[CalloutInfo("PrisonerTransport", CalloutProbability.High)]
    //public class PrisonerTransport : Callout
    //{
    //    private LocationInformation _pickupLocation;
    //    private LocationInformation _dropoffLocation;
    //    private Ped _policeOfficer;
    //    private Ped _policeOfficer2;
    //    private Ped _prisoner;
    //    private Blip _pickupBlip;
    //    private Blip _dropoffBlip;
    //    private TransportStatus _transportStatus = TransportStatus.WaitingPickup;
    //    private LHandle _pursuit;
    //    private bool _prisonerWillFight;
    //    //private Group _passengerGroup;
    //    private Group _passengerGroup;
    //    private readonly Group _playerGroup = Game.LocalPlayer.Character.Group;

    //    public override bool OnCalloutAccepted()
    //    {
    //        var random = new Random(DateTime.Now.Hour + DateTime.Now.Minute + DateTime.Now.Second + DateTime.Now.Millisecond + DateTime.Now.Day + DateTime.Now.Month);

    //        //Get the puckup location
    //        var pickupLocationIndex = random.Next(Utility.PoliceStations.Count);
    //        _pickupLocation = Utility.PoliceStations[pickupLocationIndex];

    //        //Get the destination 
    //        var dropoffLocationIndex = random.Next(Utility.PoliceStations.Count);
    //        var dropoffLocationSentry = 0;
    //        while (dropoffLocationIndex == pickupLocationIndex)
    //        {
    //            if (dropoffLocationSentry > 100)
    //                throw new ApplicationException("Tried more than 100 times to get a dropoff location");

    //            dropoffLocationIndex = random.Next(Utility.PoliceStations.Count);
    //            dropoffLocationSentry++;
    //        }
    //        _dropoffLocation = Utility.PoliceStations[dropoffLocationIndex];

    //        //Create the passengers
    //        //Rage.Game.Log("Creating the police ped");
    //        _policeOfficer = new Ped("S_M_Y_Cop_01", _pickupLocation.WaitingArea.Around(2f), 360f);
    //        _policeOfficer.GiveNewWeapon("WEAPON_PUMPSHOTGUN", 250, true);

    //        _policeOfficer2 = new Ped("S_M_Y_Cop_01", _pickupLocation.WaitingArea.Around(2f), 360f);
    //        _policeOfficer2.GiveNewWeapon("WEAPON_PUMPSHOTGUN", 250, true);

    //        //Rage.Game.Log("Creating the prisoner");
    //        _prisoner = new Ped("S_M_Y_Prisoner_01", _pickupLocation.WaitingArea.Around(2f), 360f);
    //        if (random.Next(10) > 7)
    //        {
    //            _prisoner.GiveNewWeapon("WEAPON_KNIFE", 1, false);
    //            _prisonerWillFight = true;
    //        }
    //        else
    //        {
    //            _prisonerWillFight = false;
    //        }

            
    //        //Attach a blip to the prisoner
    //        _pickupBlip = new Blip(_pickupLocation.LandingZone);

    //        return base.OnCalloutAccepted();
    //    }

    //    public override void OnCalloutNotAccepted()
    //    {
    //        Game.LogVerbose("Call to OnCalloutNotAccepted");
    //        Cleanup();
    //        base.OnCalloutNotAccepted();
    //    }

    //    private bool _waitingForPassengers;
    //    private bool _passengersDeparting;
    //    public override void Process()
    //    {
    //        try
    //        {
    //            switch (_transportStatus)
    //            {

    //                case TransportStatus.WaitingPickup:
    //                    //If we're close to the LZ, we're near the ground we can now switch to waiting for passengers
    //                    if (!_waitingForPassengers && Game.LocalPlayer.Character.Position.DistanceTo(_pickupLocation.LandingZone) < 50f && Game.LocalPlayer.LastVehicle.HeightAboveGround < 1.5f)
    //                    {
    //                        _waitingForPassengers = true;

                            
    //                        Game.LogVerbose("Set _waitingForPassengers to true");

    //                        Game.LogVerbose("The chopper has landed awating pickup");

    //                        Game.LogVerbose("_passengerGroup created and player added as leader");
    //                        _playerGroup.AddMember(_policeOfficer);
    //                        _playerGroup.SetPedMemberVehicleSeatIndex(_policeOfficer, 0);
    //                        Game.LogVerbose("_policeOfficer added to group with seatindex 0");
    //                        _playerGroup.AddMember(_policeOfficer2);
    //                        _playerGroup.SetPedMemberVehicleSeatIndex(_policeOfficer2, 1);
    //                        Game.LogVerbose("_policeOfficer2 added to group with seatindex of 1");
    //                        _playerGroup.AddMember(_prisoner);
    //                        _playerGroup.SetPedMemberVehicleSeatIndex(_prisoner, 2);
    //                        Game.LogVerbose("_prisoner added to group with seatindex of 2");

    //                        _pickupBlip.Delete();
    //                        Game.LogVerbose("_waypointBlip deleted");

    //                        _dropoffBlip = new Blip(_dropoffLocation.LandingZone);
    //                        Game.LogVerbose("_waypointBlip set to the dropoff location");

    //                        _transportStatus = TransportStatus.Enroute;
                            
    //                    }
    //                    break;

    //                case TransportStatus.Enroute:
    //                    //If we're close to the destination LZ and close to the ground
    //                    if (!_passengersDeparting && Game.LocalPlayer.Character.Position.DistanceTo(_dropoffLocation.LandingZone) < 50f && Game.LocalPlayer.LastVehicle.HeightAboveGround < 1.5f)
    //                    {
    //                        _passengersDeparting = true;
    //                        Game.LogVerbose("_passengersDeparting set to true");

                            
    //                        _playerGroup.RemoveMember(_policeOfficer);
    //                        _playerGroup.RemoveMember(_policeOfficer2);
    //                        _playerGroup.RemoveMember(_prisoner);

    //                        if (_prisonerWillFight)
    //                        {
    //                            _pursuit = Functions.CreatePursuit();
    //                            Functions.AddPedToPursuit(_pursuit, _prisoner);
    //                            Game.LogVerbose("_prisoner added to pursuit");
    //                            _prisoner.Tasks.FightAgainstClosestHatedTarget(50f, 10000);

    //                            _policeOfficer.Tasks.FightAgainst(_prisoner);
    //                            Game.LogVerbose("Ordered _policeOfficer to fight against _prisoner");

    //                            _policeOfficer2.Tasks.FightAgainst(_prisoner);
    //                            Game.LogVerbose("Ordered _policeOfficer2 to fight against _prisoner");
    //                        }
    //                        else
    //                        {
    //                            _passengerGroup = new Group(_policeOfficer);
    //                            _passengerGroup.AddMember(_policeOfficer2);
    //                            _passengerGroup.AddMember(_prisoner);
    //                            Game.LogVerbose("Officers and prisoner added to the _passengerGroup");

    //                            _policeOfficer.Tasks.GoStraightToPosition(_dropoffLocation.WaitingArea.Around(5f), 2.0f, 260f, 0f, 30000);
    //                            Game.LogVerbose("_policeOfficer ordered to goto waiting area");
    //                        }

    //                        _transportStatus = TransportStatus.DroppedOff;
    //                    }
    //                    break;

    //                case TransportStatus.DroppedOff:
    //                    if (_pursuit != null || !Functions.IsPursuitStillRunning(_pursuit))
    //                    {
    //                        End();
    //                        Game.LogVerbose("The pursuit is no longer running, ending the callout");
    //                    }
    //                    break;
    //            }

    //            base.Process();
    //        }
    //        catch (Exception e)
    //        {
    //            Game.LogVerbose(string.Format("{0}\r\n{1}\r\n{2}", e.Message, e.Source, e.StackTrace));
    //        }
    //    }

    //    public override void End()
    //    {
    //        Game.LogVerbose("Call to End");
    //        Cleanup();
    //        base.End();
    //    }

    //    private void Cleanup()
    //    {
    //        if (_policeOfficer != null && _policeOfficer.Exists())
    //            _policeOfficer.IsPersistent = false;
    //        if (_policeOfficer2 != null && _policeOfficer2.Exists())
    //            _policeOfficer2.IsPersistent = false;
    //        if (_prisoner != null && _prisoner.Exists())
    //            _prisoner.IsPersistent = false;
    //        if (_prisoner != null && _pickupBlip.Exists())
    //            _pickupBlip.Delete();
    //        if (_dropoffBlip != null && _dropoffBlip.Exists())
    //            _dropoffBlip.Delete();
    //    }
    //}
}
