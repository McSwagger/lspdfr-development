﻿using Rage;
using System;
using System.Collections.Generic;

namespace SimCopter
{
    public class LocationInformation
    {
        public string Name { get; set; }
        public Vector3 LandingZone { get; set; }
        public Vector3 WaitingArea { get; set; }
    }



    public static class Utility
    {
        public static List<LocationInformation> PoliceStations = new List<LocationInformation> 
        { 
            //Vespuchi Beach
            new LocationInformation{
                Name = "Vespucci Beach PD",
                LandingZone = new Vector3 { X = -1095.597f, Y = -835.3549f, Z = 37.67538f },
                WaitingArea = new Vector3 { X = -1106.891f, Y = -834.28f, Z = 37.67537f }
            },

            //Prison
            new LocationInformation{
                Name = "State Prison",
                LandingZone = new Vector3 { X= 1860.307f,Y=2602.892f, Z=45.67202f },
                WaitingArea = new Vector3 { X=1851.3f,Y=2605.927f,Z=45.64296f }
            },

            new LocationInformation
            {
                Name = "Police Station Downtown",
                WaitingArea = new Vector3(452.324f, -992.6896f, 43.69165f),
                LandingZone = new Vector3(449.2513f,-981.254f,43.98683f)
            },
            new LocationInformation
            {
                Name="Vinewood Police Station",
                LandingZone = new Vector3(579.702f, 12.13125f, 103.2336f),
                WaitingArea = new Vector3(570.7308f,5.057468f,103.2336f)
            },
            new LocationInformation
            {
                Name="Los Santos Sherrif and Impound",
                LandingZone = new Vector3(362.7616f,-1598.36f,36.94879f),
                WaitingArea = new Vector3(377.5467f,-1600.867f,36.94881f)
            },
            new LocationInformation
            {
                Name = "NOOSE Building",
                LandingZone = new Vector3(2510.546f,-341.6085f,118.1864f),
                WaitingArea = new Vector3(2513.562f,-338.0743f,118.0253f)

            }
        };
        public static List<LocationInformation> VipLocations = new List<LocationInformation>
        {
            //LOMBank Building
            new LocationInformation
            {
                Name = "LOMBank Building",
                LandingZone = new Vector3 { X = -1582.351f, Y = -569.1951f, Z = 116.3276f},
                WaitingArea = new Vector3 { X= -1564.432f, Y = -563.7084f, Z = 114.4494f }
            },

            new LocationInformation
            {
                Name = "Maze Bank Branch",
                LandingZone = new Vector3 {X=-1391.611f, Y = -477.5081f, Z = 91.25079f},
                WaitingArea = new Vector3 { X = -1376.957f, Y = -480.9449f, Z = 89.44549f }
            },

            new LocationInformation
            {
                Name = "Richard Majestic Building",
                LandingZone = new Vector3(-913.3831f, -378.9241f, 138.0085f),
                WaitingArea = new Vector3(-899.7104f,-374.8145f,136.2823f)
            },
            new LocationInformation
            {
                Name="Arcadus Business Building",
                LandingZone = new Vector3(-144.3685f, -593.3449f,211.7753f),
                WaitingArea = new Vector3(-140.5701f,-589.7756f,211.7753f)
            },
            
            new LocationInformation
            {
                Name = "Maze Bank Big Building",
                LandingZone = new Vector3(-75.59091f, -819.1583f, 326.1752f),
                WaitingArea = new Vector3(-81.29788f, -822.9644f, 326.1751f)
            },
            
            new LocationInformation
            {
                Name = "Sea Port Helipads",
                LandingZone = new Vector3(-724.7908f,-1444.383f,5.000521f),
                WaitingArea = new Vector3(-711.6056f, -1426.882f, 5.00052f)
            },
            
            new LocationInformation
            {
                Name="Los Santos International Airport",
                LandingZone = new Vector3(-1112.976f, -2883.249f, 13.94601f),
                WaitingArea = new Vector3(-1095.152f, -2894.374f,13.94631f)
            },
            new LocationInformation
            {
                Name = "Stay Frosty Factory",
                LandingZone = new Vector3(910.2924f, -1681.049f, 51.13248f),
                WaitingArea = new Vector3(911.9547f,-1694.739f,51.1305f)
            }
            
        };

        public static List<LocationInformation> HospitalLocations = new List<LocationInformation>
        {
            new LocationInformation
            {
                Name = "Central Los Santos Medical Center",
                LandingZone = new Vector3(299.1666f,-1453.082f,46.50948f),
                WaitingArea = new Vector3(319.8934f, -1449.479f, 46.50949f)
            }
        };
        public static List<LocationInformation> RooftopRescueLocations = new List<LocationInformation>();

        public static List<LocationInformation> GetRandomTransportMission()
        {
            var returnValue = new List<LocationInformation>();

            var random = new Random(DateTime.Now.Hour + DateTime.Now.Minute + DateTime.Now.Second + DateTime.Now.Millisecond);

            

            return returnValue;
        }

    }
}
