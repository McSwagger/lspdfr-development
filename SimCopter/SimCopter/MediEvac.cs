﻿using LSPD_First_Response.Mod.Callouts;
using System;
using System.Collections.Generic;
using System.Linq;
using LSPD_First_Response.Engine.Scripting.Entities;
using Rage;

namespace SimCopter
{
    [CalloutInfo("Medivac", CalloutProbability.High)]
    public class MediEvac : TransportBase
    {
        public override bool OnBeforeCalloutDisplayed()
        {
            PossibleLocations = Utility.HospitalLocations;

            //Find a location on the street to land
            var landingZone = World.GetNextPositionOnStreet(Game.LocalPlayer.Character.Position.Around(1000f));
            PickupLocation = new LocationInformation { LandingZone = landingZone, WaitingArea = landingZone };
            Game.LogVerbose(string.Format("Set landingZone to X: {0} Y: {1} Z: {2}", landingZone.X, landingZone.Y, landingZone.Z));
            DropoffLocation = Utility.HospitalLocations[0];

            //Setup the passengers
            var paramedic = new Ped("S_M_M_Paramedic_01", PickupLocation.WaitingArea.Around(2f), 360);
            var paramedic2 = new Ped("S_M_M_Paramedic_01", PickupLocation.WaitingArea.Around(2f), 360);
            var victim = new Ped("a_f_y_tourist_02", PickupLocation.WaitingArea.Around(2f), 360);

            Passengers = new List<Ped> { paramedic, paramedic2, victim };

            //Find the closest hospital location
            DropoffLocation = Utility.HospitalLocations.SingleOrDefault(l => Math.Abs(Vector3.Distance(landingZone, l.LandingZone) - Utility.HospitalLocations.Min(h => Vector3.Distance(landingZone, h.LandingZone))) < float.Epsilon);

            Game.LogVerbose(string.Format("Set DropoffLocation to {0}",
                DropoffLocation == null ? "<null>" : DropoffLocation.Name));

            CalloutMessage = "Medivac services needed";

            return base.OnBeforeCalloutDisplayed();
        }
    }
}
