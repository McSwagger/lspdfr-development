﻿using System;
using System.Collections.Generic;
using System.Linq;
using LSPD_First_Response.Engine.Scripting.Entities;
using LSPD_First_Response.Mod.Callouts;
using Rage;

namespace SimCopter
{
    public abstract class TransportBase : Callout
    {
        public List<LocationInformation> PossibleLocations { get; set; }
        public LocationInformation PickupLocation { get; set; }
        public LocationInformation DropoffLocation { get; set; }
        public List<Ped> Passengers { get; set; }
        public Blip PickupBlip { get; set; }
        public Blip DropoffBlip { get; set; }
        public Group PassengerGroup { get; set; }
        public TransportStatus TransportStatus { get; set; }
        public bool WaitingForPassengers { get; set; }
        public bool PassengersDeparting { get; set; }
        public List<string> PedModels { get; set; }
        public readonly Random Random = new Random(DateTime.Now.Day + DateTime.Now.Hour + DateTime.Now.Minute + DateTime.Now.Second + DateTime.Now.Millisecond);
        public readonly Group PlayerGroup = Game.LocalPlayer.Group;

        public override bool OnCalloutAccepted()
        {
            //Get pickup and dropoff locations
            PickupLocation = GetRandomLocation();
            DropoffLocation = GetRandomLocation(PossibleLocations.IndexOf(PickupLocation));
            
            //Attach a blip to the pickup location
            PickupBlip = new Blip(PickupLocation.LandingZone);

            //Create the peds if the hasn't created them already
            if (Passengers == null || Passengers.Count < 1)
            {
                for (var i = 0; i < Game.LocalPlayer.LastVehicle.FreePassengerSeatsCount; i++)
                {
                    var passenger = new Ped(PickupLocation.WaitingArea.Around(2.0f));

                    //If the ped models have been specified then pick one
                    if (PedModels != null && PedModels.Count > 0)
                        passenger.Model = new Model(PedModels[Random.Next(PedModels.Count)]);
                    if(Passengers == null)
                        Passengers = new List<Ped>{ passenger };
                    else 
                        Passengers.Add(passenger);
                }
            }

            return base.OnCalloutAccepted();
        }

        public override void Process()
        {
            try
            {
                switch (TransportStatus)
                {

                    case TransportStatus.WaitingPickup:
                        //If we're close to the LZ, we're near the ground we can now switch to waiting for passengers
                        if (!WaitingForPassengers && Game.LocalPlayer.Character.Position.DistanceTo(PickupLocation.LandingZone) < 50f && Game.LocalPlayer.LastVehicle.HeightAboveGround < 1.5f)
                        {
                            WaitingForPassengers = true;
                            Game.LogVerbose("Set _waitingForPassengers to true");

                            Game.LogVerbose("The chopper has landed awating pickup");

                            for (var i = 0; i < Passengers.Count; i++)
                            {
                                PlayerGroup.AddMember(Passengers[i]);
                                PlayerGroup.SetPedMemberVehicleSeatIndex(Passengers[i], i);
                            }

                            PickupBlip.Delete();
                            Game.LogVerbose("_waypointBlip deleted");

                            DropoffBlip = new Blip(DropoffLocation.LandingZone);
                            Game.LogVerbose("_waypointBlip set to the dropoff location");

                            TransportStatus = TransportStatus.Enroute;
                        }
                        break;

                    case TransportStatus.Enroute:
                        //If we're close to the destination LZ and close to the ground
                        if (!PassengersDeparting && Game.LocalPlayer.Character.Position.DistanceTo(DropoffLocation.LandingZone) < 50f && Game.LocalPlayer.LastVehicle.HeightAboveGround < 1.5f)
                        {
                            PassengersDeparting = true;
                            Game.LogVerbose("_passengersDeparting set to true");

                            //Remove the passengers from the player group
                            foreach (var ped in Passengers.Where(ped => Game.LocalPlayer.Group.IsMember(ped)))
                            {
                                Game.LocalPlayer.Group.RemoveMember(ped);

                                if(PassengerGroup == null || PassengerGroup.Count == 0)
                                    PassengerGroup = new Group(ped);
                                else
                                    PassengerGroup.AddMember(ped);
                            }

                            PassengerGroup.Leader.Tasks.GoStraightToPosition(DropoffLocation.WaitingArea, 2.0f, 360, 0, 50000);

                            End();
                        }
                        break;
                }

                base.Process();
            }
            catch (Exception e)
            {
                Game.LogVerbose(string.Format("{0}\r\n{1}\r\n{2}", e.Message, e.Source, e.StackTrace));
            }

            base.Process();
        }

        public override void End()
        {
            Cleanup();
            base.End();
        }

        public override void OnCalloutNotAccepted()
        {
            Cleanup();
            base.OnCalloutAccepted();
        }

        protected LocationInformation GetRandomLocation(int excludeIndex = -1)
        {
            var locationIndex = Random.Next(PossibleLocations.Count);
            var guardValue = 0;

            while (locationIndex == excludeIndex)
            {
                if (guardValue++ > 100)
                    throw new ApplicationException("Tried over 100 times to set a destination and failed");

                locationIndex = Random.Next(PossibleLocations.Count);
            }

            return PossibleLocations[locationIndex];
        }

        private void Cleanup()
        {
            foreach (var ped in Passengers.Where(ped => ped.Exists()))
                ped.IsPersistent = false;

            if (PickupBlip != null && PickupBlip.Exists())
                PickupBlip.Delete();

            if(DropoffBlip != null && DropoffBlip.Exists())
                DropoffBlip.Delete();
        }
    }
}
