﻿using LSPD_First_Response.Mod.API;
using Rage;

namespace SimCopter
{
    public class Main : Plugin
    {
        public override void Finally()
        {
            
        }

        public override void Initialize()
        {
            Functions.OnOnDutyStateChanged += Functions_OnOnDutyStateChanged;
            Game.LogTrivial("My plugin v1.0 loaded successfully");
        }

        void Functions_OnOnDutyStateChanged(bool onDuty)
        {
            if(onDuty)
            {
                //Functions.RegisterCallout(typeof(CarChase));
                //Functions.RegisterCallout(typeof(GangWar));
                Functions.RegisterCallout(typeof(PrisonerTransport));
                //Functions.RegisterCallout(typeof(RooftopRescue));
                //Functions.RegisterCallout(typeof(BoatRescue));
                //Functions.RegisterCallout(typeof(MountianTopRescue));
                Functions.RegisterCallout(typeof(MediEvac));
                Functions.RegisterCallout(typeof(VipTransport));
            }
        }
    }
}
